package main

import (
	"fmt"
)

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
	fmt.Println("fin")
}

// named return values
// usages:
// - panic
// - return value documentation
func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables here (like a & b)
	defer func() {
		if e := recover(); e != nil {
			// fmt.Println("ERROR:", e)
			// convert e (any) to error
			err = fmt.Errorf("%s", e)
		}
	}()

	return div(a, b), nil
	/* Miki don't like this style ☺
	q = div(a, b)
	return
	*/
}

func div(a, b int) int {
	return a / b
}
