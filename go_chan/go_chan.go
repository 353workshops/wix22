package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		/* Fix 2: Local parameter */
		i := i
		go func() {
			fmt.Println(i) // i from line 14
		}()

		/* Fix 1: Pass a parameter
		go func(n int) {
			fmt.Println(n)
		}(i)
		*/

		/*
			go func() {
				fmt.Println(i) // BUG: All goroutines use i from line 12
			}()
		*/
	}

	time.Sleep(10 * time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println(val)
	fmt.Println(sleepSort([]int{30, 10, 20})) // [10 20 30]

	go func() {
		for i := 0; i < 3; i++ {
			ch <- i
		}
		close(ch)
	}()

	for n := range ch {
		fmt.Println("ch:", n)
	}

	/*
		// for range ...
		for {
			n, ok := <- ch
			if !ok {
				break
			}
			fmt.Println("ch:", n)
		}
	*/

	n := <-ch // ch is closed
	fmt.Println("closed:", n)
	n, ok := <-ch
	fmt.Println("closed:", n, ok)
	// ch <- 7 // panic
	// close(ch) // panic

	ch1, ch2 := make(chan string), make(chan string)
	go func() {
		time.Sleep(100 * time.Millisecond)
		ch1 <- "one"
	}()
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- "two"
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Millisecond)
	defer cancel()
	select {
	case val := <-ch1:
		fmt.Println("ch1:", val)
	case val := <-ch2:
		fmt.Println("ch2:", val)
	// case <-time.After(5 * time.Millisecond):
	case <-ctx.Done():
		fmt.Println("timeout")
	}
}

/*
	sleep sort

- for every value n in values, spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

collect all values from channel to slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var out []int
	for range values {
		n := <-ch
		out = append(out, n)
	}
	return out
}

/* Channel semantics
- send/receive will block until opposite operation (*)
	- buffered channel has "n" non-blocking sends
- receive from closed channel with return zero value without blocking
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/
