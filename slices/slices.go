package main

import (
	"fmt"
	"sort"
)

func main() {
	var s1 []int
	fmt.Println("s1: len:", len(s1), "cap:", cap(s1))
	for _, v := range s1 {
		fmt.Println(v)
	}

	s2 := []int{}
	fmt.Println("s2: len:", len(s2), "cap:", cap(s2))
	fmt.Printf("s1: %#v, s2: %#v\n", s1, s2)

	size := cap(s1)
	for i := 0; i < 1000; i++ {
		s1 = append(s1, i)
		if cap(s1) != size {
			fmt.Println(size, "->", cap(s1))
			size = cap(s1)
		}
	}
	fmt.Println("s1: len:", len(s1), "cap:", cap(s1))

	/*
		var s3 []int
		for i := 0; i < 100; i++ {
			s3 = appendInt(s3, i)
		}
	*/
	var arr [5]int
	fmt.Printf("arr type: %T\n", arr)
	s5 := arr[:]
	fmt.Printf("s5 type: %T\n", s5)

	const n = 10
	s6 := make([]int, 0, n) // len=0, cap=10
	fmt.Println("s6 before:", s6)
	for i := 0; i < n; i++ {
		s6 = append(s6, i)
	}
	fmt.Println("s6 after:", s6)

	s7 := s6[:3] // :3]
	fmt.Println("s7", s7)
	s7 = append(s7, 99)
	fmt.Println("s6", s6)

	values := []float64{3, 1, 2}
	fmt.Println(median(values))
	values = []float64{3, 1, 2, 4}
	fmt.Println(median(values))
	fmt.Println(values)

	sa := []any{1, "one", 1.1}
	fmt.Println(sa)
	/*
		sa = []any{1, 2, 3}
		si := []int(sa)
		fmt.Println(si) // won't compile
	*/

	players := []Player{
		{"Rick", 10_000},
		{"Morty", 7},
	}

	// value semantics
	for _, p := range players {
		p.Score += 100
	}
	fmt.Println(players)

	// pointer(ish) semantics
	for i := range players {
		players[i].Score += 100
	}
	fmt.Println(players)
}

type Player struct {
	Name  string
	Score int
}

func median(vals []float64) float64 {
	// copy in order not to change vals
	values := make([]float64, len(vals))
	copy(values, vals)

	sort.Float64s(values)
	i := len(values) / 2
	if len(values)%2 == 1 {
		return values[i]
	}
	return (values[i-1] + values[i]) / 2

}

// Simulate built-in append
func appendInt(s []int, v int) []int {
	i := len(s)
	if len(s) < cap(s) {
		s = s[:len(s)+1]
	} else {
		size := 2 * (len(s) + 1)
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[i] = v
	return s
}
