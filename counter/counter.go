package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	// var counter int
	var counter int64

	// var mu sync.Mutex
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 10_000; j++ {
				atomic.AddInt64(&counter, 1)
				/*
					mu.Lock()
					counter++
					mu.Unlock()
				*/
				time.Sleep(time.Microsecond)
			}
		}()
	}
	wg.Wait()
	fmt.Println(counter)
}
