package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	ch1, ch2 := producer(1, 3), producer(2, 5)

	for msg := range fanIn(ch1, ch2) {
		fmt.Println(msg)
	}
}

/*
One of the greatest tragedies of life is the murder of a beautiful theory by a gang of brutal facts.
  - Le Rochefoucauld
*/
func fanIn(chans ...<-chan string) chan string {
	out := make(chan string)
	var wg sync.WaitGroup

	wg.Add(len(chans))
	for _, ch := range chans {
		ch := ch
		go func() {
			defer wg.Done()
			for val := range ch {
				out <- val
			}
		}()
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}
func producer(id, count int) chan string {
	ch := make(chan string)

	go func() {
		for i := 0; i < count; i++ {
			msg := fmt.Sprintf("%d: %d", id, i)
			time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
			ch <- msg
		}
		close(ch)
	}()

	return ch
}
