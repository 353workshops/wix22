package main

import "fmt"

func main() {
	var i Item
	fmt.Printf("%#v\n", i)

	i = Item{100, 200}
	fmt.Printf("%#v\n", i)

	i = Item{Y: 100} //, X: 200}
	fmt.Printf("%#v\n", i)

	fmt.Println(NewItem(10, 20))
	fmt.Println(NewItem(10, 2000))

	i.Move(200, 300)
	fmt.Printf("move %#v\n", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{50, 50},
	}
	fmt.Println(p1)
	fmt.Println(p1.X) //, p1.Item.X)
	p1.Move(100, 100)
	fmt.Println("p1 move", p1)

	ms := []Mover{
		&i,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Println(m)
	}

	fmt.Println(Copper)
	fmt.Printf("s: %s\n", Copper)
	fmt.Printf("v: %v\n", Copper)
	fmt.Printf("+v: %+v\n", Copper)
	fmt.Printf("#v: %#v\n", Copper)

	perms := Read | Write
	fmt.Println(perms)
	if perms&Read != 0 {
		fmt.Println("can read")
	}
}

// implement fmt.Stringer
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Flag int

const (
	Read Flag = 1 << iota
	Write
	Execute
)

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
)

// interfaces say what we need, not what we provide
// rule of thumb: accept interfaces, return types

type Mover interface {
	Move(int, int)
}

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

type Player struct {
	Name string
	//	X    float64
	Item // Player embeds Item
}

// i is called "the receiver"
// use pointer receiver when mutating
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

// func NewItem(x, y int) Item
// func NewItem(x, y int) *Item
// func NewItem(x, y int) (Item, error)
const (
	maxX = 1000
	maxY = 600
)

func NewItem(x, y int) (*Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d of bounds for %d/%d", x, y, maxX, maxY)
	}

	i := Item{
		X: x,
		Y: y,
	}
	// Go does escape analysis and will allocate i on the heap
	return &i, nil
}

type Item struct {
	X int
	Y int
}
