package main

import "fmt"

// type E error // E is a new type
// type E = error // type alias

func main() {
	// var i interface{} // go < 1.18
	var i any

	i = 7
	fmt.Println(i)
	// i += 1 // won't compile

	i = "Hi"
	fmt.Println(i)
}
