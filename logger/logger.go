package logger

import (
	"fmt"
	"io"
)

/* Option 1: Too restrictive
type Sink interface {
	io.Writer
	Sync() error
}
*/

type Syncer interface {
	Sync() error
}

type NopSyncer struct{}

func (s NopSyncer) Sync() error { return nil }

type Logger struct {
	w io.Writer
	s Syncer
}

func New(w io.Writer) *Logger {
	log := Logger{
		w: w,
		s: NopSyncer{},
	}

	if s, ok := w.(Syncer); ok {
		log.s = s
	}

	return &log
}

func (l *Logger) Info(msg string, args ...any) {
	fmt.Fprintf(l.w, "INFO: ")
	fmt.Fprintf(l.w, msg, args...)
	fmt.Fprintln(l.w)
	l.s.Sync()
}
