package nlp

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTokenize(t *testing.T) {
	text := "Who's on first?"
	expected := []string{"who", "on", "first"}
	tokens := Tokenize(text)

	require.Equal(t, expected, tokens)
}
