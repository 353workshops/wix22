package stemmer_test

import (
	"fmt"

	"git.wix.com/nlp/stemmer"
)

func ExampleStem() {
	fmt.Println(stemmer.Stem("working"))
	fmt.Println(stemmer.Stem("works"))
	fmt.Println(stemmer.Stem("worked"))

	// Output:
	// work
	// work
	// work
}
