package nlp_test

import (
	"fmt"

	"git.wix.com/nlp"
)

func ExampleTokenize() {
	text := "Who's on first?"
	tokens := nlp.Tokenize(text)
	fmt.Println(tokens)

	// Output:
	// [who on first]
}
