package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	if err := Login("joe", "ba00ka"); err != nil {
		log.Printf("error: %#v", err) // will leak
		return
	}

	/*
		var ae *AuthError
		if errors.As(err, &ae) {
			fmt.Println(ae.Login)
		}
	*/

}

func Login(user, passwd string) error {
	// var err *AuthError
	var err error
	fmt.Println("err is nil?", err == nil)
	return err
	/*
		return &AuthError{
			Time:   time.Now(),
			Login:  user,
			Reason: "Bad password",
		}
	*/
}

func (e *AuthError) Error() string {
	return e.Reason
}

type AuthError struct {
	Time   time.Time
	Login  string
	Reason string
}

func printDemo() {
	u := User{"Mr. Robot.", time.Now().UTC()}
	fmt.Println(u)
	fmt.Printf("v: %v\n", u)
	fmt.Printf("+v: %+v\n", u)
	fmt.Printf("#v: %#v\n", u)

	a, b := 1, "1"
	log.Printf("a=%v, b=%v", a, b)
	log.Printf("a=%#v, b=%#v", a, b)
}

type User struct {
	Login   string
	Created time.Time
}
