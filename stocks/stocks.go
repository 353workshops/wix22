package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	urlTemplate = "https://api.stocktwits.com/api/2/streams/symbol/%s.json"
)

func relatedStocks(symbol string) (map[string]int, error) {
	url := fmt.Sprintf(urlTemplate, symbol)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		url,
		nil,
	)
	if err != nil {
		return nil, err
	}

	// r, err := http.Get(url)
	r, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	var reply struct { // anonymous struct
		Messages []struct {
			Symbols []struct {
				Name string `json:"symbol"`
			}
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&reply); err != nil {
		return nil, err
	}

	counts := make(map[string]int)
	for _, m := range reply.Messages {
		for _, s := range m.Symbols {
			if !strings.EqualFold(s.Name, symbol) {
				counts[s.Name]++
			}
		}
	}

	return counts, nil
}

func main() {
	symbol := "AAPL"
	counts, err := relatedStocks(symbol)
	if err != nil {
		log.Fatal(err)
	}

	for sym, n := range counts {
		fmt.Printf("%-10s: %d\n", sym, n)
	}
}
