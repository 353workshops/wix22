package main

import "fmt"

func main() {
	is := []int{1, 3, 2}
	fmt.Println(Max(is))
	fmt.Println(Max[int](nil))

	fs := []float64{1, 3, 2}
	fmt.Println(Max(fs))

	cs := []Currency{1, 3, 2}
	fmt.Println(Max(cs))

	/*
		const n = 3
		// n := 3
		fmt.Println(n * is[0])
		fmt.Println(n * fs[0])
	*/

	/*
		var c Currency = 3
		fmt.Println(c * is[0])
	*/
}

func Add[T Ordered](a, b T) T {
	return a + b
}

func Sum[T Ordered](values []T) T {
	var total T
	for _, v := range values {
		total += v
	}
	return total
}

/* Data structure example
type Node[E any] struct {
	Value E
	Next  *Node[E]
}
*/

type Currency int //64

type Ordered interface {
	~int | ~float64 | ~string
}

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

/*
func MaxInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

func MaxFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}
*/
