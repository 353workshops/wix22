package main

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

type SumCase struct {
	Name string
	Args []int
	Want int `yaml:"sum"`
}

func loadSumCases(t *testing.T) []SumCase {
	// require := require.New(t)
	file, err := os.Open("testdata/sum_cases.yml")
	//require.NoError(err)
	require.NoError(t, err)
	defer file.Close()

	var tc []SumCase
	err = yaml.NewDecoder(file).Decode(&tc)
	require.NoError(t, err)
	return tc
}

var (
	inCI = os.Getenv("CI") != ""
	// Jenkins: BUILD_NUMBER
)

func FuzzAddFloats(f *testing.F) {
	f.Add(0.0, 0.0) // manual example
	fn := func(t *testing.T, a float64, b float64) {
		t.Log(a, b)
		v := Add(a, b)
		/*
			if v != a+b {
				t.Fatal(v)
			}
		*/
		if a < 0 && b < 0 {
			t.Fatal(a, b, v)
		}
	}
	f.Fuzz(fn)
}

func TestAddFloat(t *testing.T) {
	v := Add(0.2, 0.1)
	require.Equal(t, 0.3, v)
}

func TestCI(t *testing.T) {
	if !inCI {
		t.Skipf("not in CI")
	}
	t.Log("running")
}

func TestSumInts(t *testing.T) {
	/*
		if testing.Short() {
			t.Skipf("short")
		}
	*/

	t.Parallel() // mark tests as ok to parallel
	testCases := loadSumCases(t)
	for _, tc := range testCases {
		name := tc.Name
		if name == "" {
			name = fmt.Sprintf("%v", tc.Args)
		}
		t.Run(name, func(t *testing.T) {
			s := Sum(tc.Args)
			require.Equal(t, tc.Want, s)
		})
	}
}

func TestMain(m *testing.M) {
	log.Printf("setup")
	code := m.Run()
	log.Printf("teardown")
	os.Exit(code)
}
