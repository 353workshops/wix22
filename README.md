# Go Workshop

Wix ∴  2023 <br />

Miki Tebeka
<i class="far fa-envelope"></i> [miki@ardanlabs.com](mailto:miki@ardanlabs.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/) book


## Exercises

- Convert [taxi_check.go](_extra/taxi_check.go) to run with a goroutine per file. What was the speedup you got?
    - Tar file: [https://storage.googleapis.com/353solutions/c/data/taxi-sha256.tar](https://storage.googleapis.com/353solutions/c/data/taxi-sha256.tar)
- [Parallel file download](_extra/dld.md) 

---

## Day 1

### Agenda

- Serialization
- Project management
- gRPC
- Slices in depth
- Structs, methods & interfaces

### Code

- [game](game/game.go) - Structs, methods & interfaces
- [slices](slices/slices.go) - Working with slices
- [unter](unter) - gRPC
- [nlp](nlp) - Project structure
- [value](value/value.go) - JSON


[Terminal Log](_extra/day-1.log)


### Links

- Slices
    - [Slices](https://go.dev/blog/slices) & [Slice internals](https://go.dev/blog/go-slices-usage-and-internals) on the Go blog
    - [Slice tricks](https://github.com/golang/go/wiki/SliceTricks)
- [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway) - gRPC <-> REST
- [Writing gRPC interceptors in Go](https://shijuvar.medium.com/writing-grpc-interceptors-in-go-bf3e7671fe48)
- [Go gRPC Middleware](https://github.com/grpc-ecosystem/go-grpc-middleware)
- [gRPC metadata](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md)
- [gRPC status codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
- [Functional Options for Friendly APIs](https://dave.cheney.net/2014/10/17/functional-options-for-friendly-apis) by Dave Cheney
- [ghz](https://ghz.sh/) - gRPC benchmarking tool
- [gRPCurl](https://github.com/fullstorydev/grpcurl)
    - [More gRPC tools](https://github.com/grpc-ecosystem/awesome-grpc#tools)
- [gRPC status codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
- [gRPC types](https://developers.google.com/protocol-buffers/docs/proto3#scalar)
- [gRPC in Go](https://grpc.io/docs/languages/go/quickstart/)
- [gRPC](https://grpc.io/)
- [Go protobuf docs](https://pkg.go.dev/google.golang.org/protobuf)
- [Protocol Buffers](https://developers.google.com/protocol-buffers)
- [copier](https://github.com/jinzhu/copier)
- Project structure
    - [AdranLabs services](https://github.com/ardanlabs/service)
    - [bbolt](https://github.com/etcd-io/bbolt/tree/master)
- [mapstructure](https://pkg.go.dev/github.com/mitchellh/mapstructure)
- [encoding/json](https://pkg.go.dev/encoding/json)
- [Hyrum's Law](https://www.hyrumslaw.com/)
- [Effective Go](https://go.dev/doc/effective_go.html) - Read this!
- [Go standard library](https://pkg.go.dev/) - official documentation
- [Go Proverbs](https://go-proverbs.github.io/) - Think about them ☺

### Data & Other

- [Serialization slides](_extra/serialization.pdf)
- [Slices](_extra/slices.md)

---

## Day 2

### Agenda

- Interfaces in depth
- The empty interface and type assertion
- Generics
- Concurrency & channel semantics
- Timeouts & cancellations with context.Context
- Error handling & handling panics
- Testing


### Code

- [stats_test.go](stats/stats_test.go) - Testing & fuzzing
- [fan_in.go](fan_in/fan_in.go) - "fan in" pattern
- [rtb.go](rtb/rtb.go) - Using context.Context
- [counter.go](counter/counter.go) - Race conditions
- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
- [stocks.go](stocks/stocks.go) - Parsing complex JSON
- [stats.go](stats/stats.go) - Generics
- [empty.go](empty/empty.go) - The `any` (or `interface{}`)
- [logger](logger/logger.go) - Keeping interfaces small

[Terminal Log](_extra/day-2.log)


### Links

- [Year 2038 problem](https://en.wikipedia.org/wiki/Year_2038_problem)
- [Computer latency at human scale](https://twitter.com/jordancurve/status/1108475342468120576)
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup)
- [multierr](https://pkg.go.dev/go.uber.org/multierr)
- [Practical Go](https://dave.cheney.net/practical-go/presentations/gophercon-singapore-2019.html) by Dave Cheney
- Testing
    - [testing](https://pkg.go.dev/testing/)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify) - Many test utilities (including suites & mocking)
    - [Ginkgo](https://onsi.github.io/ginkgo/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
        - [testing/quick](https://pkg.go.dev/testing/quick) - Initial fuzzing library
    - [test containers](https://golang.testcontainers.org/)
- [Standard iterator interface discussion](https://github.com/golang/go/discussions/54245)
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour
- Error Handling
    - [Defer, Panic and Recover](https://go.dev/blog/defer-panic-and-recover)
    - [Go 1.13](https://go.dev/blog/go1.13-errors) error handling

### Data & Other

- [rtb.go](_extra/rtb.go)
- [taxi_check.go](_extra/taxi_check.go)
    - [taxi-sha256.tar](https://storage.googleapis.com/353solutions/c/data/taxi-sha256.tar)
- `https://api.stocktwits.com/api/2/streams/symbol/AAPL.json`
