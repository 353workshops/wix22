package main

import (
	"encoding/json"
	"testing"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"git.wix.com/unter/pb"
)

var u = pb.UpdateRequest{
	CarId:  "007",
	Driver: "Bond",
	Location: &pb.Location{
		Lat: 1,
		Lng: 2,
	},
	Time:   timestamppb.Now(),
	Status: pb.Status_FREE,
}

func BenchmarkProto(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := proto.Marshal(&u)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := json.Marshal(&u)
		if err != nil {
			b.Fatal(err)
		}
	}
}
