package main

import (
	"fmt"
	"log"
	"os"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"git.wix.com/unter/pb"
)

func main() {
	// var loc pb.Location
	/*
		loc := pb.Location{
			Lat: 1,
			Lng: 2,
		}
	*/
	u := pb.UpdateRequest{
		CarId:  "007",
		Driver: "Bond",
		Location: &pb.Location{
			Lat: 1,
			Lng: 2,
		},
		Time:   timestamppb.Now(),
		Status: pb.Status_FREE,
	}
	// fmt.Printf("loc: %s\n", &loc)

	data, err := proto.Marshal(&u)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Printf("len data: %d\n", len(data))

	// jdata, _ := json.Marshal(&u)
	jdata, _ := protojson.Marshal(&u)
	fmt.Printf("len jdata: %d\n", len(jdata))

	os.Stdout.Write(jdata)
}
