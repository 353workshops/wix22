package main

import (
	"context"
	"fmt"
	"net"
	"testing"

	"go.unter.org/unter/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestUpdate(t *testing.T) {
	req := pb.UpdateRequest{
		CarId:    "007",
		Driver:   "Bond",
		Location: &pb.Location{Lat: 1, Lng: 2},
		Time:     timestamppb.Now(),
		Status:   pb.Status_FREE,
	}

	var srv Unter
	resp, err := srv.Update(context.Background(), &req)
	if err != nil {
		t.Fatal(err)
	}

	if resp.CarId != req.CarId {
		t.Fatal(resp.CarId)
	}
}

func TestUpdateClient(t *testing.T) {
	lis, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}
	srv := grpc.NewServer(grpc.UnaryInterceptor(timeintInterceptor))
	var u Unter
	pb.RegisterUnterServer(srv, &u)
	defer srv.Stop()
	go srv.Serve(lis)

	port := lis.Addr().(*net.TCPAddr).Port
	addr := fmt.Sprintf(":%d", port)
	creds := insecure.NewCredentials()
	conn, err := grpc.DialContext(
		context.Background(), // if using WithBlock() - use a timeout here
		addr,
		grpc.WithTransportCredentials(creds),
		grpc.WithBlock(),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	c := pb.NewUnterClient(conn)

	req := pb.UpdateRequest{
		CarId:    "007",
		Driver:   "Bond",
		Location: &pb.Location{Lat: 1, Lng: 2},
		Time:     timestamppb.Now(),
		Status:   pb.Status_FREE,
	}

	resp, err := c.Update(context.Background(), &req)
	if err != nil {
		t.Fatal(err)
	}

	if resp.CarId != req.CarId {
		t.Fatal(resp.CarId)
	}

}
