package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

func main() {
	v := Value{
		Unit:   "cm",
		Amount: 17.3,
		// Time:   time.Now().UTC(),
	}
	/*
		enc := json.NewEncoder(os.Stdout)
		if err := enc.Encode(v); err != nil {
			log.Fatalf("error: %s", err)
		}
	*/
	data, err := json.Marshal(v)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	os.Stdout.Write(data)

	var v2 Value
	data = []byte(`{"unit": "cm", "amount": 12.3}`)
	if err := json.Unmarshal(data, &v2); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println("v2:", v2)

	/* zero vs missing
	- set defaults
	- use pointers
	- mapstructure: JSON -> map[string]any -> struct
	*/
	// var v3 Value
	v3 := Value{Amount: -1}
	data = []byte(`{"unit": "cm"}`)
	if err := json.Unmarshal(data, &v3); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println("v3:", v3)
}

// Implement json.Marshaler
func (v Value) MarshalJSON() ([]byte, error) {
	// step 1: convert to type known to encoding/json
	s := fmt.Sprintf("%.2f%s", v.Amount, v.Unit)
	// step 2: use json.Marshal
	return json.Marshal(s)
	// step 3: there is no step 3
}

func (v *Value) xxUnmarshalJSON(data []byte) error {
	var a float64
	var u string
	if _, err := fmt.Sscanf(string(data), "%f%s", &a, &u); err != nil {
		return err
	}
	v.Amount = a
	v.Unit = u
	return nil
}

/* What the json Encoder does
if m, ok := v.(json.Marshaler) {
	data, err = m.MarshalJSON()
} else {
	switch v.(type) {
	case int:
		...
	}
}
*/

type Value struct {
	Unit   string `json:"unit"`
	Amount float64
	// Time   time.Time
}

/* JSON <-> Go Types
string : string
boolean : bool
null : nil
number : float64, float32, int, int8 ... int64, uint ..
array : []T, []any ([]interface{})
object : struct, map[string]any

encoding/json API

JSON -> io.Reader -> Go: Decoder
Go -> io.Writer -> JSON: Encoder
JSON -> []byte -> Go: Unmarshal
Go -> []byte -> JSON: Marshal

Encoder & Decoder can stream
*/
